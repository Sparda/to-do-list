let content = document.querySelector("#content");
let task_label = document.querySelector("#task");
let add_bt = document.querySelector("#add");
let task_list = document.querySelector("#task_list");

add_bt.addEventListener('click', () => {
    task_label.value != "" ? (setTask(task_label.value), task_label.value = "") : window.alert("Insert something in the label.");
})

function setTask(text) {
    let li = document.createElement('li');
    let div = document.createElement('div');
    div.innerText = text;
    div.style.width = "235px";
    div.style.border = "none";
    li.style.display = "flex";
    let span = document.createElement('span');
    span.innerText = 'x';
    span.classList.add("close");
    let bt = document.createElement('button');
    bt.innerText = "Edit";
    bt.style.height = "15px";
    bt.style.fontSize = "15px";
    bt.style.paddingBottom = "20px";
    bt.style.backgroundColor = "#bfc2be";
    bt.style.marginLeft = "65px";
    bt.style.textDecoration = "none";
    bt.addEventListener('click', () => {
        let s = window.prompt("Editando tarefa...");
        if (s != null) {
            div.innerText = s;
        }
    })
    li.append(div);
    li.append(bt);
    li.append(span);
    div.addEventListener('click', () => {
        li.classList.contains("checked") ? (li.classList.remove("checked"), div.style.textDecoration = "none") : (li.classList.add("checked"), div.style.textDecoration = "line-through");
    });
    span.addEventListener('click', () => {
        li.remove();
    });
    task_list.append(li);
}